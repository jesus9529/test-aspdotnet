﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestCampaign.Models
{
    public class Campaign
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string TenantName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Active { get; set; }
    }
}