﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestCampaign.Models
{
	public class Token
	{
		public int Id { get; set; }
		[Required]
		public string TokenId { get; set; }
		public DateTime ExpirationDate { get; set; }
		public bool Active { get; set; }
	}
}