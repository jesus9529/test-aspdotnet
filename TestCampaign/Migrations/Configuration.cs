﻿namespace TestCampaign.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TestCampaign.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TestCampaign.Data.TestCampaignContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TestCampaign.Data.TestCampaignContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Campaigns.AddOrUpdate(item => item.Id,
                new Campaign() {
                    Id = 1,
                    Name = "Test Campaign",
                    TenantName = "testcampaign_432e-32d-3qd",
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    Active = true
                }

                );
        }
    }
}
